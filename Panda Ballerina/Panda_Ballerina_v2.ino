const int midden = 60; 
const int rechts = 30;
const int links = 100;
long randWait;
long randAngle; 

/*
Using Simple Servo here:
http://digistump.com/board/index.php/topic,444.0.html
https://github.com/digistump/DigisparkArduinoIntegration/tree/master/libraries/DigisparkSimpleServo
*/
#include <SimpleServo.h>
SimpleServo servo;        // create servo object to control a servo
const int servo_pin = 0;  // pin on which the servo is attached


void setup()
{
  servo.attach( servo_pin );  // attaches the servo on the defined pin to the servo object
  servo.write( midden );
  pinMode(4, OUTPUT);
  randomSeed(analogRead(0));


}

void move(int from, int to) {
  if (to > from) {  
    for (int pos=from; pos<to; pos++) {
      servo.write(pos);
      delay(15);
    }
  } else {
     for (int pos=from; pos>to; pos--) {
      servo.write(pos);
      delay(15);
    }
  }    


  
}

void loop()
{

  randWait = random(0, 4);
  randAngle = random(0, 15);
  // servo.write( links - randAngle );
  move(midden, links - randAngle);
  digitalWrite(4, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);  
  // servo.write( midden );
  move(links - randAngle, midden);
  digitalWrite(4, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(randWait * 1000);  
  // servo.write( rechts - randAngle );
  move(midden, rechts - randAngle);
  digitalWrite(4, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(randWait * 1000);  
  // servo.write( midden );
  move(rechts - randAngle, midden);
  digitalWrite(4, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(randWait * 1000);  
}
